# Rm9yaW52ZXN0
A repository which demonstrates the following instructions:

### PART 1
1. Launch 2 ec2 instances web-server01, web-server02: Instance type: t2.micro, AMI: Debian 11 (give tags to instances)
2. Install Docker Engine version 20.10.23
3. Manually install nginx image version 1.23.0 as a docker container on web-server01, web-server02
4. Launch an ansible EC2 instance (Instance type: t2.micro, AMI: Debian 11)
5. Install ansible version 2.9.15 and configure it?
6. Test connectivity of ansible instance to both web-servers (use key based auth)
7. Attach screenshot of the ansible output
![ansible ping](./images/part1.JPG)


### PART 2
1. Create ansible project structure to deploy new version of nginx image on web-server01, web-server02
2. Use ansible dynamic inventory
3. Update nginx to version 1.23.4 in both ec2 instances using rolling update
4. Attach screenshot of ansible output
![ansible-playbook](./images/part2.JPG)
